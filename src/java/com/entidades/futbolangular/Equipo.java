/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades.futbolangular;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliana
 */
@Entity
@Table(name = "equipo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipo.findAll", query = "SELECT e FROM Equipo e")
    , @NamedQuery(name = "Equipo.findByIdequipo", query = "SELECT e FROM Equipo e WHERE e.idequipo = :idequipo")
    , @NamedQuery(name = "Equipo.findByNombreequipo", query = "SELECT e FROM Equipo e WHERE e.nombreequipo = :nombreequipo")
    , @NamedQuery(name = "Equipo.findByDirectortecnico", query = "SELECT e FROM Equipo e WHERE e.directortecnico = :directortecnico")})
public class Equipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idequipo")
    private Short idequipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombreequipo")
    private String nombreequipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "directortecnico")
    private String directortecnico;

    public Equipo() {
    }

    public Equipo(Short idequipo) {
        this.idequipo = idequipo;
    }

    public Equipo(Short idequipo, String nombreequipo, String directortecnico) {
        this.idequipo = idequipo;
        this.nombreequipo = nombreequipo;
        this.directortecnico = directortecnico;
    }

    public Short getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(Short idequipo) {
        this.idequipo = idequipo;
    }

    public String getNombreequipo() {
        return nombreequipo;
    }

    public void setNombreequipo(String nombreequipo) {
        this.nombreequipo = nombreequipo;
    }

    public String getDirectortecnico() {
        return directortecnico;
    }

    public void setDirectortecnico(String directortecnico) {
        this.directortecnico = directortecnico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idequipo != null ? idequipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipo)) {
            return false;
        }
        Equipo other = (Equipo) object;
        if ((this.idequipo == null && other.idequipo != null) || (this.idequipo != null && !this.idequipo.equals(other.idequipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.futbolangular.Equipo[ idequipo=" + idequipo + " ]";
    }
    
}
