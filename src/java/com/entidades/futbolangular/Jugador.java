/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades.futbolangular;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliana
 */
@Entity
@Table(name = "jugador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jugador.findAll", query = "SELECT j FROM Jugador j")
    , @NamedQuery(name = "Jugador.findByIdjugador", query = "SELECT j FROM Jugador j WHERE j.idjugador = :idjugador")
    , @NamedQuery(name = "Jugador.findByNombrejugador", query = "SELECT j FROM Jugador j WHERE j.nombrejugador = :nombrejugador")
    , @NamedQuery(name = "Jugador.findByApellidojugador", query = "SELECT j FROM Jugador j WHERE j.apellidojugador = :apellidojugador")
    , @NamedQuery(name = "Jugador.findByEdad", query = "SELECT j FROM Jugador j WHERE j.edad = :edad")})
public class Jugador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "idjugador")
    private String idjugador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombrejugador")
    private String nombrejugador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellidojugador")
    private String apellidojugador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "edad")
    private short edad;

    public Jugador() {
    }

    public Jugador(String idjugador) {
        this.idjugador = idjugador;
    }

    public Jugador(String idjugador, String nombrejugador, String apellidojugador, short edad) {
        this.idjugador = idjugador;
        this.nombrejugador = nombrejugador;
        this.apellidojugador = apellidojugador;
        this.edad = edad;
    }

    public String getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(String idjugador) {
        this.idjugador = idjugador;
    }

    public String getNombrejugador() {
        return nombrejugador;
    }

    public void setNombrejugador(String nombrejugador) {
        this.nombrejugador = nombrejugador;
    }

    public String getApellidojugador() {
        return apellidojugador;
    }

    public void setApellidojugador(String apellidojugador) {
        this.apellidojugador = apellidojugador;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idjugador != null ? idjugador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jugador)) {
            return false;
        }
        Jugador other = (Jugador) object;
        if ((this.idjugador == null && other.idjugador != null) || (this.idjugador != null && !this.idjugador.equals(other.idjugador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.futbolangular.Jugador[ idjugador=" + idjugador + " ]";
    }
    
}
