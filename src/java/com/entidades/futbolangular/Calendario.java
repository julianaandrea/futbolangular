/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades.futbolangular;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliana
 */
@Entity
@Table(name = "calendario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Calendario.findAll", query = "SELECT c FROM Calendario c")
    , @NamedQuery(name = "Calendario.findByIdcalendario", query = "SELECT c FROM Calendario c WHERE c.idcalendario = :idcalendario")
    , @NamedQuery(name = "Calendario.findByLugarevento", query = "SELECT c FROM Calendario c WHERE c.lugarevento = :lugarevento")
    , @NamedQuery(name = "Calendario.findByFechayhora", query = "SELECT c FROM Calendario c WHERE c.fechayhora = :fechayhora")
    , @NamedQuery(name = "Calendario.findByEquipolocal", query = "SELECT c FROM Calendario c WHERE c.equipolocal = :equipolocal")
    , @NamedQuery(name = "Calendario.findByEquipovisitante", query = "SELECT c FROM Calendario c WHERE c.equipovisitante = :equipovisitante")
    , @NamedQuery(name = "Calendario.findByNumerofecha", query = "SELECT c FROM Calendario c WHERE c.numerofecha = :numerofecha")})
public class Calendario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcalendario")
    private Short idcalendario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lugarevento")
    private String lugarevento;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "fechayhora")
    private String fechayhora;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "equipolocal")
    private String equipolocal;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "equipovisitante")
    private String equipovisitante;

    @Basic(optional = false)
    @NotNull
    @Column(name = "numerofecha")
    private short numerofecha;

    public Calendario() {
    }

    public Calendario(Short idcalendario) {
        this.idcalendario = idcalendario;
    }

    public Calendario(Short idcalendario, String lugarevento, String fechayhora, String equipolocal, String equipovisitante, short numerofecha) {
        this.idcalendario = idcalendario;
        this.lugarevento = lugarevento;
        this.fechayhora = fechayhora;
        this.equipolocal = equipolocal;
        this.equipovisitante = equipovisitante;
        this.numerofecha = numerofecha;
    }

    public Short getIdcalendario() {
        return idcalendario;
    }

    public void setIdcalendario(Short idcalendario) {
        this.idcalendario = idcalendario;
    }

    public String getLugarevento() {
        return lugarevento;
    }

    public void setLugarevento(String lugarevento) {
        this.lugarevento = lugarevento;
    }

    public String getFechayhora() {
        return fechayhora;
    }

    public void setFechayhora(String fechayhora) {
        this.fechayhora = fechayhora;
    }

    public String getEquipolocal() {
        return equipolocal;
    }

    public void setEquipolocal(String equipolocal) {
        this.equipolocal = equipolocal;
    }

    public String getEquipovisitante() {
        return equipovisitante;
    }

    public void setEquipovisitante(String equipovisitante) {
        this.equipovisitante = equipovisitante;
    }

    public short getNumerofecha() {
        return numerofecha;
    }

    public void setNumerofecha(short numerofecha) {
        this.numerofecha = numerofecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcalendario != null ? idcalendario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calendario)) {
            return false;
        }
        Calendario other = (Calendario) object;
        if ((this.idcalendario == null && other.idcalendario != null) || (this.idcalendario != null && !this.idcalendario.equals(other.idcalendario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.futbolangular.Calendario[ idcalendario=" + idcalendario + " ]";
    }

}
